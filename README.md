# Boterle (Bob bot v2)

[*Markdown help*](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

### Feature Requests

**Bot Replacement**

We have to many bots. I would like to replace some of the bots with this one.

* Cancer Bot - Some functionality for meme purposes. 
* Toasty - The main use of Toasty seems to be pokemon so I doubt that will be
  touched
* Dyno - Perfect for mostly Admin stuff. If the bot could have some cooldown
  for links/images/text as well as some auditing (edits before/after and
members joining) then we could remove Dyno as well.

**API's**

* https://github.com/toddmotto/public-apis
* https://github.com/abhishekbanthia/Public-APIs
* https://cat-fact.herokuapp.com/facts


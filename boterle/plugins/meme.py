import json
from discord.ext import commands
from requests_futures.sessions import FuturesSession

from .base import BasePlugin


class MeMePlugin(BasePlugin):
    @commands.group(pass_context=True,
                    description="Get a dank meme: noris, trump")
    async def meme(self, ctx):
        if ctx.invoked_subcommand is None:
            await self.bot.say('I can\'t meme about nothing!')

    @meme.command(description="Chuck Noris Quote",
                  brief="Chuck Noris Quote")
    async def noris(self):
        session = FuturesSession()
        try:
            response = session.get('https://api.chucknorris.io/jokes/random').result()
        except:
            await self.bot.say("The Chuck Noris service is down :(")
        content = json.loads(response.content)
        await self.bot.say(content['value'])


def setup(bot):
    bot.add_cog(MeMePlugin(bot))

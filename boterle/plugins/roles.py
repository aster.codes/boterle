from discord.utils import get

from bot import client
from utils import strip_id


@client.command(name='join',
                description='Join a notification role',
                brief='Join a notification role',
                pass_context=True)
async def join_role(context, role_name):
    if role_name:
        role = get(context.message.server.roles, id=await strip_id(role_name))
        author = context.message.author
        await client.add_roles(context.message.author, role)
        await client.say("Added {} to the role {}".format(
            author.mention,
            role
        ))


@client.command(name='leave',
                description='Leave a notification role',
                brief='Leave a notification role',
                pass_context=True)
async def leave_role(context, role_name):
    if role_name:
        role = get(context.message.server.roles, id=await strip_id(role_name))
        author = context.message.author
        await client.remove_roles(context.message.author, role)
        await client.say("Removed {} from the role {}".format(
            author.mention,
            role
        ))


@client.command(name='list_roles',
                descriptions='List all of the current joinable roles',
                brief='List all of the current joinable roles',
                pass_context=True)
async def list_roles(context):
    roles = context.message.server.roles
    output = ""
    for role in roles:
        if "Notification" in str(role):
            output += str(role) + "\n"
    await client.say(output)

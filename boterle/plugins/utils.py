from discord.ext import commands

from .base import BasePlugin


class UtilsPlugin(BasePlugin):
    #    @commands.command(name="change_playing",
    #                      description="Changes the 'Playing '
    # setting for the bot",
    #                      pass_context=True)
    #    async def change_playing(self, context):
    #        await self.bot.change_presence(game=Game(name(
    @commands.command(name="dump_messages",
                      description="Admin command to dump all saved messages",
                      pass_context=True)
    async def dump_messages(self, context):
        await self.bot.say("Message Dump Begin!")
        for message in self.bot.messages:
            await self.bot.say("---")
            await self.bot.say(message.timestamp)
            await self.bot.say(message.author)
            await self.bot.say(message.clean_content)

    @commands.command(name="dump",
                      description="Dump an attribute of the bot.",
                      pass_context=True)
    async def dump(self, context, variable=None):
        if variable is not None:
            dumpable = getattr(self.bot, str(variable), None)
            if dumpable:
                await self.bot.say("```\n {}\n ```".format(dumpable))
            else:
                await self.bot.say(
                    "I can't find anything like " + str(variable)
                )
        else:
            await self.bot.say("I need something to check for!")

    @commands.command(name='whereami',
                      description='A little bit of info on what this server is',
                      pass_context=True)
    async def whereami(self, ctx):
        output = ("**Welcome!**\n"
                  "This is is the offical discord of the Game Theory Subreddit"
                  " (https://www.reddit.com/r/GameTheorists/)'\n"
                  "The rules are a lot more relaxed here than on the subreddit,"
                  " but even so please make to follow them:\n"
                  "1. No Spamming.\n"
                  "2. Don't be a dick.\n"
                  "3. No NSFW Content\n"
                  "4. No Self Promotion Without Consent of a Moderator\n"
                  "5. No Ban/Kick Evasion\n"
                  "6. No brigading. Unruly users from other subs will be banned"
                  " and reported, users caught brigading from our discord will "
                  "be banned and reported as well.")
        await self.bot.dm_author(ctx, output)


def setup(bot):
    bot.add_cog(UtilsPlugin(bot))

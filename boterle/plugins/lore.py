from discord.ext import commands

from utils import strip_id
from settings import WHO_IS
from .base import BasePlugin


class LorePlugin(BasePlugin):
    @commands.command(name='whois',
                      description='Find out about some users in the discord')
    async def whois(self, user=None):
        if user:
            if user == 'dave':
                user_id = 'dave'
            else:
                user_id = await strip_id(str(user))
            if user_id in WHO_IS.keys():
                await self.bot.say(WHO_IS.get(user_id))
            else:
                await self.bot.say(WHO_IS.get('else'))
        else:
            await self.bot.say("Who do you want to know about? (@ them!)")


def setup(bot):
    bot.add_cog(LorePlugin(bot))

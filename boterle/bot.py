import datetime
import os

from discord import Forbidden
from discord.ext.commands import Bot

from pluginbase import PluginBase
from settings import BOT_PREFIX
from settings import COGS
from settings import MAX_MESSAGES


class Boterle(Bot):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        here = os.path.dirname(os.path.abspath(__file__))
        plugin_dir = os.path.join(here, 'plugins')
        self.plugin_base = PluginBase(package='plugins')
        self.plugin_source = self.plugin_base.make_plugin_source(searchpath=[
            plugin_dir,
        ])
        for plugin in COGS:
            plugin = self.plugin_source.load_plugin(plugin)
            plugin.setup(self)

    async def get_start_time(self):
        self.start_time = datetime.datetime.now()

    async def dm_author(self, ctx, message):
        try:
            await self.send_message(ctx.message.author, message)

        except Forbidden:
            await self.send_message(message)


client = Boterle(messages=MAX_MESSAGES, command_prefix=BOT_PREFIX)
